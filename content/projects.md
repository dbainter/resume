+++
title = "Projects"
slug = "projects"
+++

# This Website
This website has given me a bit more trouble than I was really expecting, although that might be because I decided I needed a CI pipeline and for it to be free. This site is using something called Gitlab Pages, and a framework called Hugo. The framework gives you access to a variety of features like themes, markdown parsing, etc. The CI pipeline is a bit more of a pain point, but I figured it out. Now I just need to get all my images to load right
https://gitlab.com/dbainter/resume

# AFK Robot
![robot](../images/IMG_2818.gif#robot) 
I might be using the term 'robot' a bit loosely here. I strung up my xbox controller to a little motor that was plugged into a raspberry pi. On the pi I was just runing a simple script that would pull on the joy stick every so often to keep me logged into a game and not get kicked for idling when I got up to make food. This was my solution to the long wait times to log in when an expansion for a game I played released.

#  Social Security Exploit
When I was in college I took an ethical hacking class, and we were tasked with either finding an existing exploit or making a fabircated one for a presentation. We noticed that on the university's "forgot password" page for students it would only ask for first name, last name, birthday, the last 4 digits of their SSN, and a recaptcha check. The recaptcha check was painfully out of date so my partner on the project wrote a script to break the recaptcha with decent accuracy. We then tried to get one of our information with it by itterating from 0000 to 9999, and it worked. So We did a bit more research into SSNs and found that for our demographic, social security numbers had the format of Area Code - Group Number - Random 4. 

So we figured out that the area code is determined by where you were born, and learned that in the state of Florida due to the Sunshine Law you can request demographic information for students. So we did, and we got every single students first name, last name, birthday, and if they were in-state or out-of-state along with what the out of state information was. So for these students we could interpolate one of 4 to 7 area codes for their SSN. Then we found that for every state the social security agency releases the highest group number that was assigned that month. So with a students birthday we would have a range of group numbers.

Combine all this with a script and it's easy to see the problem. We stopped at this point because things started feeling a bit too skeevy. However, it made for a great presentation


# Patient Detection Reader
![patient](https://gitlab.com/dbainter/resume/-/raw/master/static/images/PatientView2.png?ref_type=heads#patient) 
This was my senior capstone project when I was in college. We created a system where a program would read if a patient was about to get out of their hospital bed and warn nurses of this occurrence through both software and hardware. My responsibility in this project was to make a website using react and firebase that could live-update when a raspberry pi pushed an update to the firebase DB. 
https://gitlab.com/winter-haven-cs/patient-detection-reader

# Automation Library
Probably the most relevant project, the automation library. I've actually done this a few times for a few different reasons and to express quarrels I've had with existing tools like katalon, test complete, postman, etc. 

I wrote out a framework using python and commandline flags to create a backend and front end validating automation tool for large scale. The idea was to have the tool connect to a database, then use some selenium scripts that did some action on a front end and to record the api call being made. Then once that api call returned `success:true` check the database to make sure that everything was recorded or altered properly. The primary issues with custom solutions seems to always be organization and usability. This entire system was code only, and trying to make objects that could be used cross script wound up with a similar system to the object repository in katalon but without a UI. For me this system works exceptionally well, however I can see how many would find this kind of organization confusing.

# Kitchen Island Build
![island](../images/island.png#patient) 
This is something I did recently. If you've ever heard the phrase 'those who work with their minds rest with their hands' that describes me to a T. I love crafting new things like tables, resin casted objects, or just building out and maintaining a garden

# Lanfire Album
A good friend of mine wrote a few songs for his now wife while they were dating. I went through and remastered his songs, added accompaniment, and polished them so he could have them pressed into a vinyl to give her the night before their wedding.

This was one of my favorite projects to work on. I recommend taking a listen if you have some time, my favorite song is probably Freezing:
https://open.spotify.com/album/3loXLIf3JlOV7TT4r0HJwf?si=KDPNQ8V7QCqCRozH54F0pg

