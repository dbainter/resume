+++
title = "Photos"
slug = "photos"
date = "2024-06-24"
aliases = ["photo", "photo-portfolio"]
author = "Dakota Bainter"
+++  

# Some background
Originally I was planning on going to school for graphic design, but ultimately chose to dive into tech. 
To this day I work on many forms of art, and I've taken some commissions.

# Commission work
![image](https://gitlab.com/dbainter/resume/-/raw/master/static/images/DSC_0790.JPG?ref_type=heads) 
I was hired to take some photos for a private concert of the band Sevendust

![image](../images/lauderdaleskyline_Alternate.jpg) 
A friend of mine threw me some cash to make a colorful edit of the Fort Lauderdale skyline for him to hang in his dorm room

![image](../images/shirt5print.jpg) 
I made this in college for the IEEE club


# Food
![image](../images/spanishChickenAndRice2.jpg) 

![image](../images/breakfastSammy4.jpg) 

![image](../images/DSC_0114.jpg) 

# Nature
![image](../images/DSC_0004[1].jpg) 

![image](../images/DSC_0127[1].jpg) 