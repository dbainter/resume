+++
title = "About Me"
description = "Quality and Realiablity first"
date = "2024-06-16"
aliases = ["about-me", "about-dakota", "dakota"]
author = "Dakota Bainter"
+++

![collegeMe](https://gitlab.com/dbainter/resume/-/raw/master/static/images/collegemefitted.png?ref_type=heads#aboutMe)


H̴̼͔͍͔̰̏̒̑͗̔̚ͅe̵̡̲͓̲̮͛̈́͋̊̈̊̐̋̔͜y̴̧͈͕̠̥̠̬̬̦̦̲̤̺̋̿͠,̶̬͕̺͊̈́̏̾̐͐̆͠ ̷̬͚̘̺̹̤̟͍̤̤́̽̀̂͑̕͝h̷͓͎͙̤̬̞͙̟͍̲̻̬̟͉́̿̌̓̀̿̕ŏ̷̹̹̜͉̜̖̫͖̱̮͉̮̿̓̓͗̉͘ͅw̴̨͈̗͉̦̼̮̑́̎̀͐̐̇̎̎̓̆̕͜͠'̷̢̜̞̝̥̝̭̣̦̻̈̾̑̅͒s̵̘͍̳͚̱̣̥̪̻̘͂̇̍̅̐͆̕ͅ ̸̛̣̈͂͊̈́̋̌͗̏̑̾͗͘͠į̴̛͉͚̣̭̇̏̄̋͑͊͛͒͆̈͆̿͝ṯ̵̢̧̛̟̜͎̰̩̪̃̄ ̵̲̤̣͚̱̪̺̥͑̃̾͝ͅg̵̳̖̲̈́͗̽͠͝o̵̦̳̘̭͋͛̋̉͛͐̏̆̅i̵̢̛̼̺̦̹̤͌̈́̾͒̂͑̓͘n̵̨͔̖͈̻̥̍̆̽͗́̈͛̌̊͊̆͒́̒̌g̴̢̡͔̱̖͓͙̫̟̙͓̗͊͒͜?̷̧̖̙͕̦͖̤̟̭̪͙̘̬̺͚̌̋͌͒̔̈́͆̕


`import English from Languages`



Whew. That's better
Hey, how's it going?
I'm Dakota. A self motivated individual who *thrives* off of the energy of a team. I enjoy working with self motivated individuals to work towards a common goal: making the establishment in which we reside something we can be proud of.

Through my work and personal experience I've found that I have a deep passion for learning. I dont want to just make sure things work, I want to know why they work. I try and find a solution to a problem before I bring it up with others, because when QA is made up of developers then the software you make is plus ultra.

# Past Experience and what I can do for you
To start off, let's talk about ✨college✨; where I learned I'm very bad at calculus and how to program. I went to Florida Polytechnic University. During my time I spent **a lot** of time doing projects. Intro to Engineering had me writing deliverables for a project to give the school solar, data structures had an assignment to read the input of weather data to predict where a hurricane would go, and I got to work with an actual hospital to create software that would detect when patients get out of bed.

In the past 5 years I've worked on ITSM software that connected IT departments to the staff they work with by providing them with machine learning driven tools and software. I was on the testing and automation team, providing support to the machine learning developers by asking their models 'Is a computer lasagna?' and observing how this question was infact, a logic bomb.

More recently, I've worked with freight professionals at Veritread. With my testing experieince I've been able to provide them top notch testing to bring the products to the next level. I work with a team of very talented people, and I believe the work speaks for itself: https://www.veritread.com/carrier/

# The things I'd love to work on
![resin](../images/resinturn.png#resin) 

Throughout my time as a professional Dakota I've found that I have a particular fondness for building, creating, and maintaining projects, but more than anything I've found that I love to learn. I spend a lot of time learning new skills, taking on new hobbies, and putting myself in the position of a beginner who's in a bit too deep over and over again. I find that everything is more related than one might think, and that you're looking for the right way to do things, not deciding them. 

At this point in my career I'm hoping to get into something new and exciting. I want to build hardware/software hybrid solutions, design a product, or take on a whole new software industry. I've worked with the backend of ITSM with sunview software prior to the serviceaide days, in a small studio with a talented small team taking independent clients, and in the freight industry to help connect shipping customers to the carriers that can help them.